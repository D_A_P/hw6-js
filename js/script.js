'use strict';

const product = {
    productName: "Телевізор Sony",
    price: "65000",
    discount: "20",    

    getPrice: function (){
       return product.price - product.price * product.discount / 100 + " UAH";
    }
}
console.log(`${product.productName} зі знижкою ${product.discount}% коштує ${product.getPrice()}`);



let person = {
    namePerson:  prompt(`Введіть своє ім'я`),
    age: prompt(`Введіть свій вік`),
};
function greeting (person) {    
    return;    
} 
greeting();
alert(`Привіт ${person.namePerson}, вам ${person.age} років`);




let example = {
    text: "Education",
    value: 100,
    car: {
        model: "Honda",
        color: "white",
    }
}

function clonExample(example) {
    let cloned = {};

    for(let i in example) {
        if (typeof example[i] === "object") {
            cloned[i] = clonExample(example[i]);
        } else {
            cloned[i] = example[i];
        }
    }
    return cloned;
}

let exampleOne = clonExample(example);
exampleOne.text = "book";
exampleOne.car.color = "black";

console.log(example);
console.log(exampleOne);
